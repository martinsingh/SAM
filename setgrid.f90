subroutine setgrid

! Initialize vertical grid

use vars	
use params

implicit none
	
real latit, long
integer i,j,it,jt,k, kmax

if(nrestart.eq.0) then

 
 if(doconstdz) then

    z(1) = 0.5*dz
    do k=2,nz
     z(k)=z(k-1)+dz
    end do

 else

    open(8,file='./grd',status='old',form='formatted') 
    do k=1,nz     
      read(8,fmt=*,end=111) z(k)
      kmax=k
    end do
    goto 222
111 do k=kmax+1,nz
     z(k)=z(k-1)+(z(k-1)-z(k-2))
    end do
222 continue
    close (8)

 end if 

end if
 	
if(.not.doconstdz) dz = 0.5*(z(1)+z(2))

do k=2,nzm
   adzw(k) = (z(k)-z(k-1))/dz
end do
adzw(1) = 1.
adzw(nz) = adzw(nzm)
adz(1) = 1.
do k=2,nzm-1
   adz(k) = 0.5*(z(k+1)-z(k-1))/dz
end do
adz(nzm) = adzw(nzm)
zi(1) = 0.
do k=2,nz
   zi(k) = zi(k-1) + adz(k-1)*dz
end do

if(LES) then
  do k=1,nzm
     grdf_x(k) = dx**2/(adz(k)*dz)**2
     grdf_y(k) = dy**2/(adz(k)*dz)**2
     grdf_z(k) = 1.
  end do
else
  do k=1,nzm
     grdf_x(k) = min(16.,dx**2/(adz(k)*dz)**2)
     grdf_y(k) = min(16.,dy**2/(adz(k)*dz)**2)
     grdf_z(k) = 1.
  end do
end if

do k=1,nzm
  gamaz(k)=ggr/cp*z(k)
end do

! MS include beta plane (as in kzm changes) 
! Also include darefactor
! darefactor is squared because earth radius is reduced and rotation rate
! increased by DARE scaling
if(dofplane) then

  if(fcor.eq.-999.) fcor = 4*pi/tau_day*sin(latitude0*pi/180.)*darefactor*darefactor ! MS using tau_day ! MS use darefactor
  fcorz = 0.
  if(docoriolisz) fcorz =  sqrt(4.*(2*pi/(tau_day)*darefactor)**2-fcor**2) ! MS using tau_day ! MS use darefactor
  fcory(:) = fcor
  fcorzy(:) = fcorz
  longitude(:,:) = longitude0

elseif(dobetaplane) then


 call task_rank_to_index(rank,it,jt)

 do j=0,ny
!     fcory(j)= 4.*pi/86400.*sin(latit*pi/180.)*darefactor*darefactor
     fcory(j)= 4.*pi/tau_day/r_earth*cos(latitude0*pi/180.)*dy &
           *(float(j+jt-(ny_gl+YES3D-1)/2)-0.5)*darefactor*darefactor ! Using r_earth, tau_day and darefactor
     fcorzy(j) = 0.
 end do



else
! Full sinusiodal dependence of f on latitude (also incudes darefactor
 call task_rank_to_index(rank,it,jt)

 do j=0,ny
     latit=latitude0+dy*(j+jt-(ny_gl+YES3D-1)/2-0.5)/(2*pi*r_earth)*360.*darefactor ! MS using r_earth and darefactor
     fcory(j)= 4.*pi/tau_day*sin(latit*pi/180.)*darefactor ! MS using tau_day
     fcorzy(j) = 0.
     if(j.ne.0.and.docoriolisz) fcorzy(j) = sqrt(4.*(2*pi/(tau_day)*darefactor)**2-fcory(j)**2) ! MS using tau_day
 end do

end if ! dofplane

if (doradlat) then
 call task_rank_to_index(rank,it,jt)
 do j=1,ny
     latitude(:,j) = latitude0+dy*(j+jt-(ny_gl+YES3D-1)/2-0.5)/(2*pi*r_earth)*360.*darefactor ! MS using r_earth ! MS added darefactor (kzm)
  end do
else
  latitude(:,:) = latitude0
end if

if (doradlon) then
 call task_rank_to_index(rank,it,jt)
 do i=1,nx
     longitude(i,:) = longitude0+dx/cos(latitude0*pi/180.)* &
                            (i+it-nx_gl/2-0.5)/(2*pi*r_earth)*360.*darefactor ! MS using r_earth ! MS added darefactor (kzm)
  end do
else
  longitude(:,:) = longitude0
end if


end
