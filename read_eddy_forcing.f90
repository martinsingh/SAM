! This is a subroutine to read in forcing files for 
! Temperature, zonal wind and humidity
! as a function of latitude and height
! 
! It is based on the readiopdata.f90 file 


subroutine read_eddy_forcing( error_code )



  use vars, only: ugls, dqls, dtls,time_out,dT_out,dq_out,du_out

  use grid
  use params, only: rgas, cp, fac_cond, fac_sub, longitude0,latitude0,pi,r_earth 


   implicit none

   include 'netcdf.inc'

!------------------------------Inputs-----------------------------------

   integer error_code        ! returns netcdf errors

!------------------------------Locals-----------------------------------
!     
   integer nlev, nlat, ierr, n,it,jt,ntime

   ! dimensions
   real, allocatable       ::    lat_in(:),lev_in(:) ! dimensions

   ! soundings, omega, advective tendencies (only function of time, lev here)
   real, allocatable :: du_in(:,:,:), dT_in(:,:,:), dq_in(:,:,:)

   integer NCID, STATUS
   integer lev_dimID,  lev_varID, lat_dimID, lat_varID

   integer k, m, kk, i,j, idummy, idummy2

   logical use_nf_real           ! nctype for 4byte real

   character(len=8) lowername
   character(len=120) forcingfilepath

   real    yy
   real, parameter :: missing_value = -99999.

   logical :: get_add_surface_data

! USE_4BYTE_REAL
   use_nf_real = .true.

!     
!     Open Forcing dataset
!     
   forcingfilepath = './'//trim(forcingfile) 
   if(masterproc) write(*,*) 'Opening ', forcingfilepath
   STATUS = NF_OPEN( forcingfilepath, NF_NOWRITE, NCID )
   if ( STATUS .NE. NF_NOERR ) then
      if(masterproc) write( 6,* ) &
           'ERROR(read_eddy_focring.f90):Cant open eddy forcing dataset: ' ,forcingfilepath
      call task_abort() 
   end if

!     
!======================================================
!     read level data
!     
   call get_netcdf_dimlength(ncid, 'z', nlev, status, .true.)

   ALLOCATE(lev_in(nlev),STAT=status)
   if(status.ne.0) then
      write(6,*) 'Could not allocate z in read_eddy_forcing'
      call task_abort()
   end if

   ! get pressure levels (in Pascal)
   call get_netcdf_var1d_real( NCID, 'z', lev_in, use_nf_real, status,.true. )

!     
!======================================================
!     read lat data
!     
   call get_netcdf_dimlength(ncid, 'lat', nlat, status, .true.)

   ALLOCATE(lat_in(nlat),STAT=status)
   if(status.ne.0) then
      write(6,*) 'Could not allocate lat in read_eddy_forcing'
      call task_abort()
   end if

   ! get latitude
   call get_netcdf_var1d_real( NCID, 'lat',lat_in,use_nf_real,status,.false. )


!======================================================
!     read time data
!     
   call get_netcdf_dimlength(ncid, 'time', ntime, status, .false.)

   ALLOCATE(time_out(ntime),STAT=status)
   time_out = 0.0
   if(status.ne.0) then
      write(6,*) 'Could not allocate time in read_eddy_forcing'
      call task_abort()
   end if

   if ( ntime .gt. 1 ) then
     ! get time
     call get_netcdf_var1d_real( NCID, 'time',time_out,use_nf_real,status,.false. )
   endif



!         
!====================================================================
!     allocate variables
!     
   Allocate(dT_in(nlat,nlev,ntime), dq_in(nlat,nlev,ntime),du_in(nlat,nlev,ntime), STAT=status)

   if(status.ne.0) then
      write(6,*) 'Could not allocate input variables in read_eddy_forcing'
      call task_abort()
   end if
!
!====================================================================
!     read variables with pressure and time dependence (q,T,etc.)
!     
   ! Temperature tendencies
   dT_in(:,:,:) = missing_value
   if ( ntime .eq. 1) then
     call get_netcdf_var2d_real( ncid,'dTdt',nlat,nlev,dT_in(:,:,1), &
        use_nf_real,status,.true.)
   else
     call get_netcdf_var3d_real( ncid,'dTdt',nlat,nlev,ntime,dT_in, &
        use_nf_real,status,.true.)
   endif

   ! humidity tendencies
   dq_in(:,:,:) = missing_value
   if ( ntime .eq. 1) then
     call get_netcdf_var2d_real( ncid,'dqdt',nlat,nlev,dq_in(:,:,1), &
        use_nf_real,status,.true.)
   else
     call get_netcdf_var3d_real( ncid,'dqdt',nlat,nlev,ntime,dq_in, &
        use_nf_real,status,.true.)
   endif


   ! zonal wind tendencies
   du_in(:,:,:) = missing_value
   if ( ntime .eq. 1) then
     call get_netcdf_var2d_real( ncid,'dudt',nlat,nlev,du_in(:,:,1), &
        use_nf_real,status,.true.)
   else
     call get_netcdf_var3d_real( ncid,'dudt',nlat,nlev,ntime,du_in, &
        use_nf_real,status,.true.)
   endif


!====================================================================
!     INterpolate variables to model grid
!     

   if(masterproc) print*,'Eddy forcing data read.'
   allocate(ugls(ny,nzm), dTls(ny,nzm),dqls(ny,nzm),STAT=ierr)
   ugls = 0.0
   dTls = 0.0
   dqls = 0.0
   allocate(du_out(ny,nzm,ntime), dT_out(ny,nzm,ntime),dq_out(ny,nzm,ntime),STAT=ierr)
   if(ierr.NE.0) then
      if(masterproc) then
         write(*,*) 'Error in allocating output vars in read_eddy_forcing'
      end if
      call task_abort()
   end if

     call task_rank_to_index(rank,it,jt)

     do j=1,ny
       ! This is the latitude of the model scalar points
       yy=latitude0+dy*(j+jt-(ny_gl+YES3D-1)/2-0.5)/(2*pi*r_earth)*360.*darefactor
! latitude ! MS darefactor
       do k=1,nlat
         if ( yy<=lat_in(k) ) then
              du_out(j,:,:) = du_in(k-1,:,:) + ( du_in(k,:,:)-du_in(k-1,:,:) )/(lat_in(k)-lat_in(k-1) )*( yy-lat_in(k-1) )
              dT_out(j,:,:) = dT_in(k-1,:,:) + ( dT_in(k,:,:)-dT_in(k-1,:,:) )/(lat_in(k)-lat_in(k-1) )*( yy-lat_in(k-1) )
              dq_out(j,:,:) = dq_in(k-1,:,:) + ( dq_in(k,:,:)-dq_in(k-1,:,:) )/(lat_in(k)-lat_in(k-1) )*( yy-lat_in(k-1) )
            goto 56 ! I am so sorry. 
 
         endif
       enddo
       write(*,*) 'ERROR: latitude values of input forcing do not span model latitudes (high)'
       call task_abort()
 
56     continue
       if ( k .eq.1 ) then
         write(*,*) 'ERROR: latitude values of input forcing do not span model latitudes (low)'
         call task_abort()
       endif
     enddo



   deallocate(du_in,dT_in,dq_in,STAT=status)
   if(status.ne.0) then
      write(6,*) 'Processor ', rank, &
           'Could not de-allocate forcing arrays in read_eddy_forcing'
      call task_abort()
   end if

   return
contains
  !=====================================================================
  subroutine get_netcdf_dimlength( NCID, dimName, dimlength, status, required)
    implicit none
    include 'netcdf.inc'

    ! input/output variables
    integer, intent(in)   :: NCID
    character, intent(in) :: dimName*(*)
    logical, intent(in) :: required

    integer, intent(out) :: status, dimlength

    ! local variables
    integer :: dimID

    ! get variable ID
    STATUS = NF_INQ_DIMID( NCID, dimName, dimID )
    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find dimension ID for ', &
               dimName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note(read_eddy_forcing.f90): No dimension ID for ', dimName
          dimlength=1
          return
       endif
    endif

    STATUS = NF_INQ_DIMLEN( NCID, dimID, dimlength )
    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find length of ',dimName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note - read_eddyforcing.f90 : Could not find length of ',&
               dimName
               dimlength=1
       endif
    endif

  end subroutine get_netcdf_dimlength
  !=====================================================================
  subroutine get_netcdf_var1d_real( NCID, varName, var, use_nf_real, &
       status, required)
    implicit none
    include 'netcdf.inc'

    ! input/output variables
    integer, intent(in)   :: NCID
    character, intent(in) :: varName*(*)
    logical, intent(in) :: required, use_nf_real

    integer, intent(out) :: status
    real, intent(inout) :: var(:)

    ! local variables
    integer :: varID

    ! get variable ID
    STATUS = NF_INQ_VARID( NCID, varName, varID )
    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find variable ID for ', &
               varName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note(read_eddy_forcing.f90): Optional variable ', varName,&
               ' not found in ', TRIM(iopfile)
          return
       endif
    endif

    if (use_nf_real) then
       STATUS = NF_GET_VAR_REAL( NCID, varID, var )
    else
       STATUS = NF_GET_VAR_DOUBLE( NCID, varID, var )
    endif

    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find variable ', varName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note (read_eddy_forcing.f90) : Could not find ', varName
       endif
    endif

  end subroutine get_netcdf_var1d_real
  !=====================================================================
  subroutine get_netcdf_var2d_real( NCID, varName, nlat, nlev, &
       var, use_nf_real, status, required)
    !based on John Truesdale's getncdata_real_1d
    implicit none
    include 'netcdf.inc'

    ! input/output variables
    integer, intent(in)   :: NCID, nlat, nlev
    character, intent(in) :: varName*(*)
    logical, intent(in) :: required, USE_NF_REAL

    integer, intent(out) :: status
    real, intent(inout) :: var(:,:)

    ! local variables
    integer :: varID
    character     dim_name*( NF_MAX_NAME )
    integer     var_dimIDs( NF_MAX_VAR_DIMS )
    integer     start(5 ), count(5 )
    integer     var_ndims, dim_size, dims_set, i, n, var_type
    logical usable_var

    ! get variable ID
    STATUS = NF_INQ_VARID( NCID, varName, varID )
    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find variable ID for ',&
               varName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note(read_eddy_forcing.f90): Optional variable ', varName,&
               ' not found in ', TRIM(iopfile)
          return
       endif
    endif

!
! Check the var variable's information with what we are expecting
! it to be.
!
   STATUS = NF_INQ_VARNDIMS( NCID, varID, var_ndims )
   if ( var_ndims .GT. 4 ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90: The input var',varName, &
           'has', var_ndims, 'dimensions'
      STATUS = -1
      return
   endif

   STATUS =  NF_INQ_VARTYPE(NCID, varID, var_type)
   if ( var_type .NE. NF_FLOAT .and. var_type .NE. NF_DOUBLE .and. &
        var_type .NE. NF_INT ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90: The input var',varName, &
           'has unknown type', var_type
      STATUS = -1
      return
   endif

   STATUS = NF_INQ_VARDIMID( NCID, varID, var_dimIDs )
   if ( STATUS .NE. NF_NOERR ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90:Cant get dimension IDs for', varName
      return
   endif
!     
!     Initialize the start and count arrays 
!     
   do n = 1,nlev

      dims_set = 0
      do i =  var_ndims, 1, -1

         usable_var = .false.
         STATUS = NF_INQ_DIMNAME( NCID, var_dimIDs( i ), dim_name )
         if ( STATUS .NE. NF_NOERR ) then
            if(masterproc) write(6,*) &
                 'Error: getncdata.f90 - can''t get dim name', &
                 'var_ndims = ', var_ndims, ' i = ',i
            return
         endif

         ! extract all latitude in the iop file
         if ( dim_name .EQ. 'lat' ) then
            start( i ) = 1
            count( i ) = nlat
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         ! extract the single longitude in the iop file
         if ( dim_name .EQ. 'lon' ) then
            start( i ) = 1
            count( i ) = 1          
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         ! extract one times at this level
         if ( dim_name .EQ. 'time' .OR. dim_name .EQ. 'tsec'  ) then
            start( i ) = 1
            count( i ) = 1      ! Extract a single value 
            dims_set = dims_set + 1   
            usable_var = .true.
         endif

         ! extract one level with each call
         if ( dim_name .EQ. 'z' ) then
            start( i ) = n
            count( i ) = 1
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         if ( usable_var .EQV. .false. ) then
            if(masterproc) write(6,*) &
                 'ERROR - getncdata.f90: The input var ', &
                 varName, ' has an unusable dimension ', dim_name
            STATUS = -1
         endif
      end do
      if ( dims_set .NE. var_ndims ) then
         if(masterproc) write(6,*) &
              'ERROR - getncdata.f90: Could not find all the', &
              'dimensions for input var ', varName
         if(masterproc) write(6,*) &
	'Found ',dims_set, ' of ',var_ndims
         STATUS = -1
      endif

      if (use_nf_real) then
         STATUS = NF_GET_VARA_REAL( NCID, varID, start, count, var(1,n) )
      else   
         STATUS = NF_GET_VARA_DOUBLE( NCID, varID, start, count, var(1,n) )
      endif

      if (STATUS .NE. NF_NOERR ) then
         if(required) then
            if(masterproc) write(6,*) &
                 'ERROR(read_eccy_forcing.f90):Could not find variable ', &
                 varName
            STATUS = NF_CLOSE( NCID )
            call task_abort()
         else
            if(masterproc) write(6,*) &
                 'Note (read_eddy_forcing.f90) : Could not find ', varName
         endif
      endif

   end do

 end subroutine get_netcdf_var2d_real

  !=====================================================================
  subroutine get_netcdf_var3d_real( NCID, varName, nlat, nlev,ntime, &
       var, use_nf_real, status, required)
    !based on John Truesdale's getncdata_real_1d
    implicit none
    include 'netcdf.inc'

    ! input/output variables
    integer, intent(in)   :: NCID, nlat, nlev,ntime
    character, intent(in) :: varName*(*)
    logical, intent(in) :: required, USE_NF_REAL

    integer, intent(out) :: status
    real, intent(inout) :: var(:,:,:)

    ! local variables
    integer :: varID
    character     dim_name*( NF_MAX_NAME )
    integer     var_dimIDs( NF_MAX_VAR_DIMS )
    integer     start(5 ), count(5 )
    integer     var_ndims, dim_size, dims_set, i, n, var_type
    logical usable_var

    ! get variable ID
    STATUS = NF_INQ_VARID( NCID, varName, varID )
    if (STATUS .NE. NF_NOERR ) then
       if(required) then
          if(masterproc) write(6,*) &
               'ERROR(read_eddy_forcing.f90):Could not find variable ID for ',&
               varName
          STATUS = NF_CLOSE( NCID )
          call task_abort()
       else
          if(masterproc) write(6,*) &
               'Note(read_eddy_forcing.f90): Optional variable ', varName,&
               ' not found in ', TRIM(iopfile)
          return
       endif
    endif

!
! Check the var variable's information with what we are expecting
! it to be.
!
   STATUS = NF_INQ_VARNDIMS( NCID, varID, var_ndims )
   if ( var_ndims .GT. 4 ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90: The input var',varName, &
           'has', var_ndims, 'dimensions'
      STATUS = -1
      return
   endif

   STATUS =  NF_INQ_VARTYPE(NCID, varID, var_type)
   if ( var_type .NE. NF_FLOAT .and. var_type .NE. NF_DOUBLE .and. &
        var_type .NE. NF_INT ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90: The input var',varName, &
           'has unknown type', var_type
      STATUS = -1
      return
   endif

   STATUS = NF_INQ_VARDIMID( NCID, varID, var_dimIDs )
   if ( STATUS .NE. NF_NOERR ) then
      if(masterproc) write(6,*) &
           'ERROR - getncdata.f90:Cant get dimension IDs for', varName
      return
   endif
!     
!     Initialize the start and count arrays 
!     
   do n = 1,ntime

      dims_set = 0
      do i =  var_ndims, 1, -1

         usable_var = .false.
         STATUS = NF_INQ_DIMNAME( NCID, var_dimIDs( i ), dim_name )
         if ( STATUS .NE. NF_NOERR ) then
            if(masterproc) write(6,*) &
                 'Error: getncdata.f90 - can''t get dim name', &
                 'var_ndims = ', var_ndims, ' i = ',i
            return
         endif

         ! extract all latitude in the iop file
         if ( dim_name .EQ. 'lat' ) then
            start( i ) = 1
            count( i ) = nlat
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         ! extract the single longitude in the iop file
         if ( dim_name .EQ. 'lon' ) then
            start( i ) = 1
            count( i ) = 1          
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         ! extract all times at this level
         if ( dim_name .EQ. 'time' .OR. dim_name .EQ. 'tsec'  ) then
            start( i ) = n
            count( i ) = 1      ! Extract a single value 
            dims_set = dims_set + 1   
            usable_var = .true.
         endif

         ! extract one level with each call
         if ( dim_name .EQ. 'z' ) then
            start( i ) = 1
            count( i ) = nlev
            dims_set = dims_set + 1
            usable_var = .true.
         endif

         if ( usable_var .EQV. .false. ) then
            if(masterproc) write(6,*) &
                 'ERROR - getncdata.f90: The input var ', &
                 varName, ' has an unusable dimension ', dim_name
            STATUS = -1
         endif
      end do
      if ( dims_set .NE. var_ndims ) then
         if(masterproc) write(6,*) &
              'ERROR - getncdata.f90: Could not find all the', &
              'dimensions for input var ', varName
         if(masterproc) write(6,*) &
	'Found ',dims_set, ' of ',var_ndims
         STATUS = -1
      endif

      if (use_nf_real) then
         STATUS = NF_GET_VARA_REAL( NCID, varID, start, count, var(1,1,n) )
      else   
         STATUS = NF_GET_VARA_DOUBLE( NCID, varID, start, count, var(1,1,n) )
      endif

      if (STATUS .NE. NF_NOERR ) then
         if(required) then
            if(masterproc) write(6,*) &
                 'ERROR(read_eddy_forcing.f90):Could not find variable ', &
                 varName
            STATUS = NF_CLOSE( NCID )
            call task_abort()
         else
            if(masterproc) write(6,*) &
                 'Note (read_eddy_forcing.f90) : Could not find ', varName
         endif
      endif

   end do

 end subroutine get_netcdf_var3d_real



end subroutine read_eddy_forcing

