module simple_ocean

!------------------------------------------------------------
! Purpose:
!
! A collection of routines used to specify fixed 
! or compute interactive SSTs, like slab-ocean model, etc.
!
! Author: Marat Khairoutdinov
! Based on dynamic ocean impelemntation from the UW version of SAM.
!------------------------------------------------------------

use grid
implicit none




public set_sst     ! set SST 
public sst_evolve ! evolve SST according to a model set by the ocean_type

CONTAINS


SUBROUTINE set_sst()

 use vars, only: sstxy,t00,qoceanxy
 use params, only: tabs_s, delta_sst, ocean_type,lat_0,r_earth,pi,latitude0

! parameters of the sinusoidal SST destribution 
! along the X for Walker-type simulatons( ocean-type = 1):

 real(8) tmpx(nx), pii, lx, yy
 integer i,j,k, it,jt,kmax ! MS add 

 real, dimension(2000) :: sst_in,lat_in,qflux_in

 select case (ocean_type)

   case(0) ! fixed constant SST

      sstxy = tabs_s - t00

   case(1) ! Sinusoidal distribution along the x-direction:

     lx = float(nx_gl)*dx
     do i = 1,nx
        tmpx(i) = float(mod(rank,nsubdomains_x)*nx+i-1)*dx
     end do
     pii = atan2(0.d0,-1.d0)
     do j=1,ny
       do i=1,nx
         sstxy(i,j) = tabs_s-delta_sst*cos(2.*pii*tmpx(i)/lx) - t00
       end do
     end do
   
   case(2) ! Sinusoidal distribution along the y-direction:
     
     call task_rank_to_index(rank,it,jt)
     
     pii = atan2(0.d0,-1.d0)
     lx = float(ny_gl)*dy
     do j=1,ny
        yy = dy*(j+jt-(ny_gl+YES3D-1)/2-1)
       do i=1,nx
         sstxy(i,j) = tabs_s+delta_sst*(2.*cos(pii*yy/lx)-1.) - t00
       end do
     end do 


   case(3) ! MS distribution as in Lindzen & Hou (also used in Boos & Kuang, 2010)
           ! SST = SST_0 - DeltaT*( sin(lat) - sin(lat_0) )^2
    
     call task_rank_to_index(rank,it,jt)
     
     do j=1,ny
        yy=latitude0+dy*(j+jt-(ny_gl+YES3D-1)/2-0.5)/(2*pi*r_earth)*360.*darefactor ! latitude ! MS darefactor
       do i=1,nx
         sstxy(i,j) = tabs_s-delta_sst*( sin(pi*yy/180)-sin(pi*lat_0/180) )**2 - t00
       end do
     end do 


    case(4) ! MS read in SST from file ! Also read qflux from file. 


     call task_rank_to_index(rank,it,jt)

     ! For now just use latitudinal dependence
     ! read same file for each longitude
     ! This is awful code built to a very specific thing. If you want to read in
     ! more general SST inputs, probably should write someting better.
      open(8,file='./SST',status='old',form='formatted')
      kmax=0
      do k=1,2000 ! Maximum number of records is 2000
        read(8,fmt=*,end=55) sst_in(k),qflux_in(k),lat_in(k)
        kmax = kmax+1
      end do
55    continue
      close(8) 

     ! now that we have the SST forcing loaded, interpolate to the model values
     do j=1,ny
       yy=latitude0+dy*(j+jt-(ny_gl+YES3D-1)/2-0.5)/(2*pi*r_earth)*360.*darefactor ! latitude ! MS darefactor
       do k=1,kmax
         if ( yy<=lat_in(k) ) then
            do i=1,nx
              sstxy(i,j) = sst_in(k-1) + ( sst_in(k)-sst_in(k-1) )/( lat_in(k)-lat_in(k-1) )*( yy-lat_in(k-1) )-t00 
              qoceanxy(i,j) = qflux_in(k-1) + ( qflux_in(k)-qflux_in(k-1) )/( lat_in(k)-lat_in(k-1) )*( yy-lat_in(k-1) )
            enddo
            goto 56
 
         endif
       enddo
       write(*,*) 'ERROR: latitude values of input SST do not span model latitudes (high)'
       call task_abort()
 
56     continue
       if ( k .eq.1 ) then
         write(*,*) 'ERROR: latitude values of input SST do not span model latitudes (low)'
         call task_abort()
       endif
     enddo

    
       write(*,*) 'sst forced'
       write(*,*) sstxy(1,:)+t00
       write(*,*) 'qflux if used'
       write(*,*) qoceanxy(1,:)
 
   case default

     if(masterproc) then
         print*, 'unknown ocean type in set_sst. Exitting...'
         call task_abort
     end if

 end select









end subroutine set_sst



SUBROUTINE sst_evolve
 use vars, only: sstxy, t00, fluxbt, fluxbq, rhow,qocean_xy,qoceanxy
 use params, only: cp, lcond, tabs_s, ocean_type, dossthomo, &
                   depth_slab_ocean, Szero, deltaS, timesimpleocean,diff_sst ! MS diff
 use rad, only: swnsxy, lwnsxy

 real, parameter :: rhor = 1000. ! density of water (kg/m3)
 real, parameter :: cw = 4187.   ! Liquid Water heat capacity = 4187 J/kg/K
 real factor_cp, factor_lc
 real tmpx(nx), lx
 real sst_diff_tend(nx,ny) ! MS diff tend
 real(8) sss,ssss
 integer i,j

      if(time.lt.timesimpleocean) return

      lx = float(nx_gl)*dx
      do i = 1,nx
        tmpx(i) = float(mod(rank,nsubdomains_x)*nx+i-1)*dx
      end do

      ! Define weight factors for the mixed layer heating due to
      ! the model's sensible and latent heat flux.
      factor_cp = rhow(1)*cp
      factor_lc = rhow(1)*lcond

      ! Use forward Euler to integrate the differential equation
      ! for the ocean mixed layer temperature: dT/dt = S - E.
      ! The source: CPT?GCSS WG4 idealized Walker-circulation 
      ! RCE Intercomparison proposed by C. Bretherton.

      do j=1,ny
         do i=1,nx
           ! MS: To deal with the weird oscilations in latitude of the surface
           ! temperature, include some temperature diffusion
           sst_diff_tend(i,j) =  diff_sst/darefactor/darefactor * ( ( sstxy(i+1,j)-2*sstxy(i,j)+sstxy(i-1,j) )/(dx**2) &
                                       +  ( sstxy(i,j+1)-2*sstxy(i,j)+sstxy(i,j-1) )/(dy**2) )
         enddo
      enddo



      do j=1,ny
         do i=1,nx
           !qoceanxy = Szero + deltaS*abs(2.*tmpx(i)/lx - 1)
	   qocean_xy(i,j) = qocean_xy(i,j) + qoceanxy(i,j)

            sstxy(i,j) = sstxy(i,j) &
                 + dtn*sst_diff_tend(i,j)   & ! tendency from diffusion
                 + dtn*(swnsxy(i,j)          & ! SW Radiative Heating
                 - lwnsxy(i,j)               & ! LW Radiative Heating
                 - factor_cp*fluxbt(i,j)/darefactor     & ! Sensible Heat Flux
                 - factor_lc*fluxbq(i,j)/darefactor     & ! Latent Heat Flux !MS These have already been multiplied by dare factor so need to divide them here 
                 + qoceanxy(i,j))            & ! Ocean Heating
                 /(rhor*cw*depth_slab_ocean)        ! Convert W/m^2 Heating to K/s
         end do
      end do

     if(dossthomo) then
        sss = 0.
        do j=1,ny
         do i=1,nx
           sss = sss + sstxy(i,j)
         end do
        end do
        sss = sss / dble(nx*ny)
        if(dompi) then
            call task_sum_real8(sss,ssss,1)
            sss = ssss /float(nsubdomains)
        end if ! dompi
        if(ocean_type.ge.2) then
            tabs_s = sss + t00
            call set_sst()
        else
           sstxy(:,:) = sss
        end if
     end if

end subroutine sst_evolve


end module simple_ocean
