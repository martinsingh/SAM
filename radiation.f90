
subroutine radiation()

!	Radiation interface

use grid
use params, only: dosmoke, doradsimple
use rad_MS, only: rad_MS_tend
use rad_gray, only: rad_gray_tend ! At the moment this means cant use full radiation
implicit none

call t_startf ('radiation')
	
if(doradsimple.eq.1) then ! MS simple rad

!  A simple predefined radiation (longwave only)

    if(dosmoke) then
       call rad_simple_smoke()
    else
       call rad_simple()
    end if
	




elseif(doradsimple.gt.1) then ! MS simple radiation scheme

       call rad_MS_tend()

 
elseif (doradsimple.eq.-1) then ! MS Gray radiation

       call rad_gray_tend()

else ! Call full radiation package:
 

       call rad_full()	
 
endif

call t_stopf ('radiation')

end


