module rad_gray

!
! Gray radiation scheme following O'Gorman & Schneider 2008
! Gray radiation Aquaplanet MOdel *GrAM* (see also Frierson 2006)
!
! In GrAM upward and downward fluxes are calcualted separately
! with surface flux calculation occuring in between. 
! Here we just do them together for simplicity

implicit none


contains

subroutine rad_gray_init()
 
write(*,*) 'Using gray radiation: This is in beta, so hold on to your boots!' 

end subroutine rad_gray_init


subroutine rad_gray_tend()

use vars
use grid
use params, only: pi,cp,ggr,stefan,solar_constant,del_sol,del_sw,ir_tau_eq,ir_tau_pole,atm_abs,sw_diff, &
                  linear_tau,albedo_value,window,wv_exponent,solar_exponent
use rad, only: qrad,lwnsxy,swnsxy,lwntxy,swntxy,lwnscxy,swnscxy,lwntcxy,swntcxy,solinxy

real, dimension(ny) :: ss, solar, tau_0, solar_tau_0, p2
real, dimension(nx,ny) :: b_surf
real, dimension(ny,nz) :: tau,solar_tau
real, dimension(ny,nzm) :: dtrans
real, dimension(nx,ny,nz) :: up,down,solar_down,net_lw,net_sw
real, dimension(nx,ny,nzm) :: b
real, dimension(ny) :: cc,tt,h0 ! MS add for seasonal cycle

integer i,j,k

! MS add time for varying solar radiation
integer :: days, seconds
real :: declination



! Should do this in the init routine to save computation
! But for now we leave here (only a 1D computation, so this is not a huge
! slowdown)
! Latitude is 2D array, ss is 1D
ss  = sin(latitude(1,:)*pi/180)
cc  = cos(latitude(1,:)*pi/180)
tt  = tan(latitude(1,:)*pi/180)

! Second Legendre polynomial
p2 = (1. - 3.*ss*ss)/4.

! MS adding seasonal cycle

if ( del_sol .lt. 0.0 ) then ! if del_sol is less than zero use realistic TOA daily average radiation


  ! Calculate the solar declination
  ! Use a 360 day calendar with 30 day months
  ! Austral Summer solstice occurs at day -10. 
  ! Also use sinusiodal approximation:
  ! cosine variation of declination angle 
  ! (see Wikipedia page on Sun position for details
  ! del_sw is the Earth's axial tilt in radians
  declination = -del_sw*cos( 2*pi*(day+10.)/(360.) )

  ! cosine of sunrise/sunset hour angle
  h0 = -tt*tan(declination)

  ! ensure cos(h0) is sensible
  where( h0 >  1.0 ) h0 =  1.0 
  where( h0 < -1.0 ) h0 = -1.0 

  ! Calculate h0
  h0 = acos(h0)

  ! Calculate latitudinally dependent solar insolation
  ! this function is the daily average solar radiation at each latitude given
  ! the solar declination, and assumng a round orbit
  ! See Hartmann (1994), Global Physical Climatology, p. 30.
  solar = 1/pi*solar_constant*( h0*ss*sin(declination) + cc*cos(declination)*sin(h0) )

else
  ! Calculate solar radiation at TOA
  solar = 0.25*solar_constant*(1.0 + del_sol*p2 + del_sw * ss)

endif

! End MS seasonal cycle changes

! Calculate LW optical depth
tau_0 = ir_tau_eq + (ir_tau_pole - ir_tau_eq)*ss*ss

! Calculate SW optial depth
solar_tau_0 = (1.0 - sw_diff*ss*ss)*atm_abs


! nzm is num levels, nz = nzm+1
do k = 1, nz

  ! Use reference pressures for optical depth calculation. Also remember that in
  ! SAM pressure is ordered high to low
  tau(:,k)       = tau_0(:) * (linear_tau * presi(k)/presi(1) + (1.0- linear_tau) &
       * (presi(k)/presi(1))**wv_exponent)

  solar_tau(:,k) =  solar_tau_0(:)*(presi(k)/presi(1))**solar_exponent

end do

! no radiation from spectral window
b = (1.0-window)*stefan*tabs*tabs*tabs*tabs




! transmission
do k = 1, nzm
  dtrans(:,k) = exp(-(tau(:,k)-tau(:,k+1)))
end do

! Calculate downward radiation (level nz is TOA) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! LW radiation
down(:,:,nz) = 0.0
do k = nzm,1,-1
  do j =1,ny
    down(:,j,k) = down(:,j,k+1)*dtrans(j,k) + b(:,j,k)*(1.0 - dtrans(j,k))
  end do
end do

! SW radiation
do i = 1, nx
do j = 1, ny
  do k = nz,1,-1
         solar_down(i,j,k) = solar(j)*exp(-solar_tau(j,k))
  end do
end do
end do



! Upward radiation !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! total flux from surface
b_surf = stefan*(sstxy(1:nx,1:ny)+t00)**4


!! first deal with non-window upward flux

! Upward flux at surface
up(:,:,1) = b_surf*(1.0-window)
do k = 1,nzm
  do j = 1,ny
    up(:,j,k+1) = up(:,j,k)*dtrans(j,k) + b(:,j,k)*(1.0 - dtrans(j,k))
  end do
end do

! add upward flux in spectral window
do k = 1,nz
 up(:,:,k) = up(:,:,k) + b_surf(:,:)*window
end do


! Net fluxes are upwards positive
do k = 1,nz
  net_lw(:,:,k) = up(:,:,k)-down(:,:,k)
  net_sw(:,:,k) = albedo_value*solar_down(:,:,1) - solar_down(:,:,k)
end do

! Calculate radiaitve tendency: note pressure is in hPa, so need to muliply by
! 100
do k = 1,nzm
  qrad(:,:,k) = (net_lw(:,:,k+1) - net_lw(:,:,k) - solar_down(:,:,k+1) + solar_down(:,:,k))  &
             *ggr/(cp*(presi(k+1)-presi(k))*100)
end do


! Upwards positive
lwnsxy = net_lw(:,:,1)                 ! LW SRF net
lwntxy = net_lw(:,:,nzm)               ! LW TOA net

! Downwards positive
swnsxy = -net_sw(:,:,1)                ! SW SRF net
swntxy = -net_sw(:,:,nzm)              ! SW TOA net

solinxy = solar_down(:,:,nz)           ! SW TOA dwn


! Set clear-sky values to all-sky values as gray radiation has no clouds
lwntcxy = lwntxy                      ! LW TOA net
lwnscxy = lwnsxy                      ! LW SRF net

swnscxy = swnsxy                      ! SW SRF net
swntcxy = swntxy                      ! SW TOA net



! Add the radiative tendency to the frozen moist static energy (note 't' is not  temperature)
do i = 1,nx
do j = 1,ny
do k = 1,nzm
   t(i,j,k) = t(i,j,k) + qrad(i,j,k) * dtn ! add radiative heating to sli
enddo
enddo
enddo

! Prepare statistics
if ( icycle .eq. 1 ) then ! Only add to output for first cycle (prevents double counting)
do j=1,ny
     do i=1,nx
        ! Net surface and toa fluxes
        lwns_xy(i,j) = lwns_xy(i,j) + lwnsxy(i,j) 
        swns_xy(i,j) = swns_xy(i,j) + swnsxy(i,j)
        lwnt_xy(i,j) = lwnt_xy(i,j) + lwntxy(i,j) 
        swnt_xy(i,j) = swnt_xy(i,j) + swntxy(i,j)
        lwnsc_xy(i,j) = lwnsc_xy(i,j) + lwnscxy(i,j)
        swnsc_xy(i,j) = swnsc_xy(i,j) + swnscxy(i,j)
        lwntc_xy(i,j) = lwntc_xy(i,j) + lwntcxy(i,j)
        swntc_xy(i,j) = swntc_xy(i,j) + swntcxy(i,j)
        ! TOA Insolation
        solin_xy(i,j) = solin_xy(i,j) + solinxy(i,j)
     end do
end do
endif


end subroutine rad_gray_tend


end module rad_gray




