# SAM 6.9.5 modified to include

1. Ability to use DARE technique (see Kuang et al., 2005).
2. Simple radiation scheme based on Newtonian relaxation
3. Gray radiation scheme from O''Gorman & Schneider
4. Option to input surface temperature as function of latitude
5. Option to input ocean heat flux as function of latitude
6. Diffusion of temperature for slab-ocean runs to remove oscillations 
7. Relax winds to zonal-mean values rather than the global-mean
8. allow zonally asymmetric SST blob
9. small fix to latitude definition for coriolis parameter
10. possibility of initialization with an aggregated state
11. seasonal cycle of insolation (gray)
12. T,q and u forcing (time invariant or seasonal)  able to be added
13. Seasonal variation in insolation 

