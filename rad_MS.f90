module rad_MS

use grid
use params, only: doradsimple, relax_profile, rad_cool, Trad1, Trad2, tau_rad, cp 
implicit none

real T_E(nx, ny, nzm)

contains

subroutine rad_MS_init()
use netcdf

integer :: ncid, varID,i,j,k
integer ::  stat   = 0
  
    if ( doradsimple.eq.3 .and. relax_profile .eq. 0 ) then ! read from input


     stat   = nf90_open('./T_E.nc',nf90_nowrite,ncid)
     if ( stat .ne.0 ) then
        write(*,*) 'ERROR: netcdf error (MS)'
        call task_abort()
     endif

     stat   = nf90_inq_varid(ncid,"Temperature",varID)
     if ( stat .ne.0 ) then
        write(*,*) 'ERROR: netcdf error (MS)'
        call task_abort()
     endif

     stat   = nf90_get_var(ncid, varID, T_E)
     if ( stat .ne.0 ) then
        write(*,*) 'ERROR: netcdf error (MS)'
        call task_abort()
     endif

    else ! simple constant temperature

       do i = 1,nx
       do j = 1,ny
       do k = 1,nzm
          T_E(i,j,k) = Trad1
       enddo
       enddo
       enddo





    endif

end subroutine rad_MS_init


subroutine rad_MS_tend()
 	
!	Simple radiation scheme by Martin Singh
!       includes constant cooling parameterization and Newtonian relaxation
!       parameetrization

use vars
use rad, only: qrad
implicit none
	

integer i,j,k
real FTHRL(nzm) 
real cpmassl(nzm) 


! Keep these for stat output
do k=1,nzm
radlwdn(k) =0.
radqrlw(k) =0.
enddo

do k = 1,nzm
   cpmassl(k) = cp*rho(k)*dz*adz(k) ! thermal mass of model layer.
end do

do i=1,nx
do j=1,ny
   do k=1,nzm

      if(  doradsimple.eq.2 ) then

        ! Constant cooling in troposphere
        if ( tabs(i,j,k) .gt. Trad2 ) then
           FTHRL(k)=-rad_cool
        elseif ( tabs(i,j,k) .gt. Trad1 ) then
           FTHRL(k) = -(tabs(i,j,k) - Trad1)/(Trad2-Trad1)*rad_cool
        else
           FTHRL(k) = 0.0
        endif

      elseif ( doradsimple.eq.3) then
   
        ! relaxation to equilibrium state
        FTHRL(k)=-( tabs(i,j,k)-T_E(i,j,k) )/tau_rad

      else
        if(masterproc) print*,'Invalid radiation option'
        if(masterproc) print*,'rad_MS.f90'
        if(masterproc) print*,'Stop.'
        call abort()
      end if


      t(i,j,k) = t(i,j,k) + FTHRL(k) * dtn ! add radiative heating to sli

      qrad(i,j,k) = FTHRL(k) ! store radiative heating for 3d output/stepout
   enddo
enddo
enddo



end subroutine rad_MS_tend


end module rad_MS




